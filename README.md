# Morpion

Ce jeu a été réalisé afin de tester mes capacités logiques et applicatives en programmation `` Python ``

## Cas d'utilisation

```plantuml

@startuml
left to right direction

actor Player
actor System

Player -> (Jouer une partie)
(Player) -> (Placer un symbole)
Bot ->(Placer un symbole)
System -> (Initialiser le jeu)
System --> (Afficher la grille)
(System) -> (Déterminer le gagnant)
(System) -> (Fin de partie)

@enduml


```  

## Pour commencer

1. Connaître les règles du jeu.
2. Transformer les règles en code

### Pré-requis

Avoir un environnement de développement en `` Python ``  

### Installation

Télécharger le répertoire

## Démarrage

Ouvrez le répertoire dans votre IDE

## Aperçu

Aperçu du jeu :

![Aperçu du jeu](assets/ap1.jpg)


## Conçu avec

* [Thonny](https://thonny.org/) - IDE de développement en Python
* [PyCharm](https://www.jetbrains.com/fr-fr/pycharm/) - IDE de développement Python
* [Python](https://www.python.org/) - Langage de programmation interprété

## Auteurs

* **Joackim SEGOR** _alias_ [@TheJoker971](https://gitlab.com/TheJoker971)

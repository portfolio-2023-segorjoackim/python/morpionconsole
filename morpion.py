import random



class Player:
    def __init__(self,signe):
        self.signe = signe
        self.name =""
        self.Ur_name()
    
    def Ur_name(self):
        self.name = input("Saisir votre pseudo : ")
    
    def choose(self,cadre):
        x = int(input("Saisir la position x entre 1 et 3 : "))-1
        y = int(input("Saisir la position y entre 1 et 3 : "))-1
        while(cadre.jouer(x,y,self.signe) != True):
            x = int(input("Saisir la position x entre 1 et 3 : "))
            y = int(input("Saisir la position y entre 1 et 3 : "))
        
    
class Bot:
    def __init__(self,signe):
        self.name = "bot1"
        self.signe = signe
        
    def choose(self,cadre):
        
        xy= self.target(cadre.cadre)
        
        while(cadre.jouer(xy[0],xy[1],self.signe) == False):
            xy= self.target(cadre.cadre)
        
        
    def target(self,cd):
        i = 1
        listA = []
        for l in cd:
            for c in l :
                globals()[f"A{i}"] = c
                listA.append(globals()[f"A{i}"])
                #print(globals()[f"A{i}"])
                i += 1
                
        #Ligne 1
        if(A1 == A2 != ["-"] and A3 == ["-"]):
            return (0,2)
        if(A2 == A3 != ["-"] and A1 == ["-"]):
            return (0,0)
        if(A1==A3 != ["-"] and A2 == ["-"]):
            return (0,1)
        
        #Ligne 2
        if(A4 == A5 != ["-"] and A6 == ["-"]):
            return (1,2)
        if(A5 == A6 != ["-"] and A4 == ["-"]):
            return (1,0)
        if(A4==A6 != ["-"] and A5 == ["-"]):
            return (1,1)
        
        #Ligne 3
        if(A7 == A8 != ["-"] and A9 == ["-"]):
            return (2,2)
        if(A8 == A9 != ["-"] and A7 == ["-"]):
            return (2,0)
        if(A7 == A9 != ["-"] and A8 == ["-"]):
            return (2,1)
        
        #Colonne 1
        if(A1 == A4 != ["-"] and A7 == ["-"]):
            return (2,0)
        if(A4 == A7 != ["-"] and A1 == ["-"]):
            return (0,0)
        if(A1 == A7 != ["-"] and A4 == ["-"]):
            return (1,0)
        
        #Colonne 2
        if(A2 == A5 != ["-"] and A8 == ["-"]):
            return (2,1)
        if(A5 == A8 != ["-"] and A2 == ["-"]):
            return (0,1)
        if(A2 == A8 != ["-"] and A5 == ["-"]):
            return (1,1)
        
        #Colonne 3
        if(A3 == A6 != ["-"] and A9 == ["-"]):
            return (2,2)
        if(A6 == A9 != ["-"] and A3 == ["-"]):
            return (0,2)
        if(A3 == A9 != ["-"] and A6 == ["-"]):
            return (1,2)
        
        #Diagonale 1
        if(A1 == A5 != ["-"] and A9 == ["-"]):
            return (2,2)
        if(A5 == A9 != ["-"] and A1 == ["-"]):
            return (0,0)
        if(A1 == A9 != ["-"] and A5 == ["-"]):
            return (1,1)
        
        #Diagonale 2
        if(A3 == A5 != ["-"] and A7 == ["-"]):
            return (2,0)
        if(A5 == A7 != ["-"] and A3 == ["-"]):
            return (0,2)
        if(A3 == A7 != ["-"] and A5 == ["-"]):
            return (1,1)
        
        
        else:
            l = random.randint(0,2)
            c = random.randint(0,2)
            return (l,c)
        
        
            
class Cadre:
    def __init__(self):
        self.cadre =[
            [["-"],["-"],["-"]],
            [["-"],["-"],["-"]],
            [["-"],["-"],["-"]]
            ]
    
    def jouer(self,x,y,signe):
        if(self.cadre[x][y] == ["-"]):
            self.cadre[x][y] = signe
            return True
        else:
            return False
        
        
    def plein(self):
        for i in range(len(self.cadre)):
            for j in self.cadre[i]:
                if(j == ["-"]):
                    return False
        return True
    
    def gagne(self):
        c = self.cadre
        for i in range(len(c)):
            if(c[i][0] == c[i][1] == c[i][2]):
                return c[i][0]
            if(c[0][i] == c[1][i] == c[2][i]):
                return c[0][i]
            if(c[0][0] == c[1][1] == c[2][2]):
                return c[0][0]
            if(c[2][0] == c[1][1] == c[0][2]):
                return c[2][0]
        return ""
    
    def end(self,players):
        signe = self.gagne()
        for i in players:
            if(i.signe == signe):
                print(f"Le gagnant est {i.name} !!!")
                return True
        return False
                
            
            
    def afficher(self):
        return f"{self.cadre[0]}\n{self.cadre[1]}\n{self.cadre[2]}"
    
def morpion():
    
    signe = ["X","O"]
    choix = int(input(f"Choisissez le signe 1 ou 2 {signe} : "))
    cadre = Cadre()
    player = Player(signe[choix-1])
    if(choix < 2):
        bot = Bot(signe[choix])
    else:
        bot = Bot(signe[choix-2])
    
    players = [player,bot]
    #players = ["X","O"]
    turn = 1
    while(cadre.plein() == False and cadre.end(players) == False):
        for i in players:
            if(cadre.plein() == False):
                i.choose(cadre)
        print(f"Turn {turn} :\n")
        print(cadre.afficher())
        turn += 1
    #print(cadre.afficher())
            
    

morpion()


